﻿from osgeo import gdal
from osgeo import osr
import re
import os
import argparse

def dms_to_dec(dms):
    deg, min, sec = dms
    if deg < 0 :
        return -(sec / 3600) - (min / 60) + deg
    else:
        return (sec / 3600) + (min / 60) + deg

def parse_dms(dms_str):
    h, m, s = re.findall("\((\d+\.{0,1}\d*)\)", dms_str)
    return float(h), float(m), float(s)

def fake_geotransform(world_center, image_res,  spatial_res):
    easting, northing = world_center[0], world_center[1]
    width_px, height_px = image_res[0], image_res[1]


    top, left = northing + height_px * spatial_res,  easting - width_px * spatial_res
    return (left, spatial_res, 0.0, top, 0.0, -spatial_res)

def change_extension(path, new_ext):
    path_, _ = os.path.splitext(path)
    return path_ + '.' + new_ext.lstrip('.')

class CoordConverter:
    def __init__(self, src_epsg, dst_epsg):

        self._src_epsg = src_epsg
        self._src_srs = osr.SpatialReference()
        self._src_srs.ImportFromEPSG(src_epsg)

        self._dst_epsg = dst_epsg
        self._dst_srs = osr.SpatialReference()
        self._dst_srs.ImportFromEPSG(dst_epsg)

        self._src_to_dst = osr.CoordinateTransformation(self._src_srs, self._dst_srs)
        self._dst_to_src = osr.CoordinateTransformation(self._dst_srs, self._src_srs)

    def src_to_dst(self, x, y):
        x_, y_ , _ = self._src_to_dst.TransformPoint(x, y)
        return x_, y_

    def dst_to_src(self, x, y):
        x_, y_ , _ = self._dst_to_src.TransformPoint(x, y)
        return x_, y_

    def src_srs(self):
       return self._src_srs 

    def dst_srs(self):
       return self._dst_srs


def build_args_parser():
    parser = argparse.ArgumentParser(
        description="Creates faked georefernce from jpg images prepared with Esccrow (geolocation in exif) software. Output images are tiled and LZW compressed geotiffs.")

    parser.add_argument("in_dir", metavar="source_directory")
    parser.add_argument("out_dir", metavar="output_directory")
    parser.add_argument("--srs", metavar="epsg_code", type=int, help="Spatial reference system to be asigned to images", required=True)
    parser.add_argument("--spatial_res", metavar="meters", type=float, default=5.0, help="Pixel size in meters on the map", required=True)
    parser.add_argument("--tiled", metavar="", type=bool, help="Use inner tiling for output images")
    parser.add_argument("--lzw", metavar="", type=bool, help="Use compression for output images")
    parser.add_argument("--tfw", metavar="", type=bool, help="Generate TFW files")
    return parser


if __name__ == "__main__":

    parser = build_args_parser()
    args = parser.parse_args()
    print args

    in_dir = args.in_dir
    out_dir = args.out_dir

    epsg_src = 4326 # WGS84
    epsg_dest = args.srs
    spatial_res = args.spatial_res

    tiffDriver = gdal.GetDriverByName("GTiff")
    tiffOptions = []
    if args.tiled:
        tiffOptions.append("TILED=YES")
    if args.lzw:
        tiffOptions.append("COMPRESS=LZW")
    if args.tfw:
        tiffOptions.append("TFW=YES")

    conv = CoordConverter(epsg_src, epsg_dest)

    log = open(os.path.join(out_dir,"__log.txt"),'w')

    files_list = os.listdir(in_dir)
    files_count = len(files_list)
    for (idx, f) in enumerate(files_list, start=1):
        
        print "{}\t[{}/{}]".format(f, idx, files_count)
        if not f.endswith(".jpg") and not f.endswith(".jpeg"):
            continue
        
        try:

            in_path = os.path.join(in_dir, f)
            out_path = os.path.join(out_dir, change_extension(f, "tif"))

            img = gdal.Open(in_path, gdal.OF_READONLY)
            meta = img.GetMetadata()
            lat = dms_to_dec(parse_dms(meta['EXIF_GPSLatitude']))
            lon = dms_to_dec(parse_dms(meta['EXIF_GPSLongitude']))
            easting, northing = conv.src_to_dst(lon, lat)
            geotrans = fake_geotransform((easting, northing), (img.RasterXSize, img.RasterYSize), spatial_res)
            img_out =tiffDriver.CreateCopy(out_path, img, options=tiffOptions)
            img_out.SetGeoTransform(geotrans)
            img_out.SetProjection(conv.dst_srs().ExportToWkt())

            img = None
            img_out = None
        except Exception as e:
            log.write(in_path)
            print e.message